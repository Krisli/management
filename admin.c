//
// Created by Ejsi on 18-06-04.
//

#include <stdbool.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include "admin.h"
#include "main.h"

char *bAdmin;

void shtoPunonjesView();
void ndryshoDataView();
void ndryshoEmerView(Punonjes punonjes);
void ndryshoFjalekalimView(Punonjes punonjes);
void ndryshoMosheView(Punonjes punonjes);
void ndryshoPageView(Punonjes punonjes);
void fshiPunonjesView();
void kerkoIdView();
void kerkoEmerView();
void kerkoPunonjesView();
void raporteView();
void listoPunonjesit();
void listoPunonjesitMosha();
void listoPunonjesitRaportim();
void listoPunonjesitPunet();
void listoMesataret();
void listoSipasPunes();
void listoSipasPages();
void listoSipasRendimentit();

bool adminHome(Admin admin) {
    int input;
    loggedAdmin = admin;
    printf("\n\nMiresevjen Admin!\n");
    printf("-------------------------------------------------------------\n");
    printf("1. Shto Punonjes\n");
    printf("2. Ndrysho te dhenat e punonjesve\n");
    printf("3. Fshi punonjes\n");
    printf("4. Kerko punonjes\n");
    printf("5. Raporte\n");
    printf("6. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 6) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 6: ");
        res = scanf("%d", &input);
    }

    switch (input) {
        case 1:
            shtoPunonjesView();
            break;
        case 2:
            ndryshoDataView();
            break;
        case 3:
            fshiPunonjesView();
            break;
        case 4:
            kerkoPunonjesView();
            break;
        case 5:
            raporteView();
            break;
        case 6:
            logout();
            break;
    }

    return true;
}

Admin getAdmin(char password[]) {
    Admin tmpAdmin;

    createConnection("admin.txt", 'r');
    loadBuffer();
    closeConnection();
    bAdmin = getBuffer();

    char *file = strdup(bAdmin);
    file = strtok(file, "|");

    char *fFields;
    int field = 0;
    while ((fFields = strsep(&file, "~"))) {
        if (strlen(fFields) > 0) {
            switch (field) {
                case 0:
                    tmpAdmin.id = parseInt(fFields, strlen(fFields));
                    break;
                case 1:
                    strcpy(tmpAdmin.fjalekalim, fFields);
                    break;
            }
        }
        field ++;
    }

    if (strcmp(password, tmpAdmin.fjalekalim) != 0) {
        tmpAdmin.id = -1;
    }

    return tmpAdmin;
}

void shtoPunonjesView() {
    char emer[20];
    char mbiemer[20];
    char fjalekalim[255];
    int mosha;
    float paga;
    bool vazhdo = true;

    while (vazhdo) {
        printf("\nJu lutem fusni te dhenat e punonjesit qe doni te shtoni:\n");
        printf("-----------------------------------------------------------\n");
        printf("Emri: ");
        scanf("%s", emer);
        printf("Mbiemri: ");
        scanf("%s", mbiemer);
        printf("Fjalekalimi: ");
        scanf("%s", fjalekalim);
        printf("Mosha: ");
        int res = scanf("%d", &mosha);
        while (res != 1 || mosha < 0) {
            getchar();
            printf("Mosha duhet te jete nje numer pozitiv!\n");
            printf("Mosha: ");
            res = scanf("%d", &mosha);
        }

        printf("Paga: ");
        res = scanf("%f", &paga);
        while (res != 1 || paga < 0) {
            getchar();
            printf("Paga duhet te jete nje numer pozitiv!\n");
            printf("Paga: ");
            res = scanf("%f", &paga);
        }

        Punonjes result = shtoPunonjes(emer, mbiemer, fjalekalim, mosha, paga, 0, 0, 0);
        if (result.id == -1) {
            printf("\nGabim gjate shtimit te punonjesit!]n");
        } else {
            printf("\nPunonjesi u shtua me sukses!");
            printPunonjes(result);
            savePunonjesit();
        }

        char input[2];
        printf("Deshironi te shtoni nje punonjes tjeter? [Y/N]\n");
        getchar();
        scanf("%s", input);
        if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    adminHome(loggedAdmin);
}

void ndryshoDataView() {
    int id;

    printf("\nJu lutem fusni ID e punonjesit qe doni te modifikoni:\n");
    printf("-------------------------------------------------------\n");
    printf("ID: ");

    bool vazhdo = true;

    Punonjes *tmp;
    while (vazhdo) {
        int res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID: ");
            res = scanf("%d", &id);
        }

        tmp = getPunonjesByIdReference(id);
        if (tmp == NULL) {
            printf("ID e gabuar!\n");
            printf("ID: ");
            vazhdo = true;
        } else if (tmp->id == -1){
            printf("ID e gabuar!\n");
            printf("ID: ");
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    int input;
    printf("Punonjesi: %s", tmp->emer);
    printf("\n-------------------------\n");
    printf("1. Ndrysho emrin\n");
    printf("2. Ndrysho fjalekalimin\n");
    printf("3. Ndrysho moshen\n");
    printf("4. Ndrysho pagen\n");
    printf("5. Kthehu mbrapa\n");
    printf("------------------------\n");
    printf("Veprimi: ");

    scanf("%d", &input);
    while (input < 1 || input > 5) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 5: ");
        scanf("%d", &input);
    }

    switch (input) {
        case 1:
            ndryshoEmerView(*tmp);
            break;
        case 2:
            ndryshoFjalekalimView(*tmp);
            break;
        case 3:
            ndryshoMosheView(*tmp);
            break;
        case 4:
            ndryshoPageView(*tmp);
            break;
        case 5:
            adminHome(loggedAdmin);
            break;
    }

    adminHome(loggedAdmin);
}

void ndryshoEmerView(Punonjes punonjes) {
    char emer[20];
    char mbiemer[20];

    printf("(Emri aktual: %s)\n", punonjes.emer);
    printf("Emri: ");
    scanf("%s", emer);
    printf("Mbiemri: ");
    scanf("%s",  mbiemer);

    char name[50] = "";
    strcat(name, emer);
    strcat(name, "_");
    strcat(name, mbiemer);

    ndryshoEmrin(punonjes.id, name);
    printf("Emri u nderrua me sukses!\n");
}

void ndryshoFjalekalimView(Punonjes punonjes) {
    char passw[255];

    printf("Fjalekalimi i ri: ");
    scanf("%s", passw);

    ndryshoFjalekalimin(punonjes.id, passw);
    printf("Fjalekalimi u nderrua me sukses!\n");
}

void ndryshoMosheView(Punonjes punonjes) {
    int mosha;

    printf("(Mosha aktuale: %d)\n", punonjes.mosha);
    printf("Mosha: ");
    int res = scanf("%d", &mosha);
    while (res != 1 || mosha < 0) {
        getchar();
        printf("Mosha duhet te jete nje numer pozitiv!\n");
        printf("Mosha: ");
        res = scanf("%d", &mosha);
    }

    ndryshoMoshen(punonjes.id, mosha);
    printf("Mosha e punonjesit u nderrua me sukses!\n");
}

void ndryshoPageView(Punonjes punonjes) {
    float paga;

    printf("(Paga aktuale: %f)\n", punonjes.paga);
    printf("Paga: ");
    int res = scanf("%f", &paga);
    while (res != 1 || paga < 0) {
        getchar();
        printf("Paga duhet te jete nje numer pozitiv!\n");
        printf("Paga: ");
        res = scanf("%f", &paga);
    }

    ndryshoPagen(punonjes.id, paga);
    printf("Paga e punonjesit u nderrua me sukses!\n");
}

void fshiPunonjesView() {
    int id;

    printf("\nJu lutem fusni ID e punonjesit qe doni te fshini:\n");
    printf("---------------------------------------------------\n");
    printf("ID: ");

    bool vazhdo = true;
    Punonjes *tmp;
    while (vazhdo) {
        int res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID: ");
            res = scanf("%d", &id);
        }

        tmp = getPunonjesByIdReference(id);
        if (tmp == NULL) {
            printf("ID e gabuar!\n");
            printf("ID: ");
            vazhdo = true;
        } else if (tmp->id == -1){
            printf("ID e gabuar!\n");
            printf("ID: ");
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    printf("Punonjesi me ID=%d (%s) duke u fshire...\n", tmp->id, tmp->emer);
    deletePunonjesById(tmp->id);
    printf("Fshirja u realizua me sukses!\n");
    adminHome(loggedAdmin);
}

void kerkoPunonjesView() {
    int input;

    printf("\nKerko punonjes\n");
    printf("-------------------------\n");
    printf("1. Kerko me ID\n");
    printf("2. Kerko me emer\n");
    printf("3. Kthehu mbrapa\n");
    printf("------------------------\n");
    printf("Veprimi: ");

    scanf("%d", &input);
    while (input < 1 || input > 3) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 3: ");
        scanf("%d", &input);
    }

    switch (input) {
        case 1:
            kerkoIdView();
            break;
        case 2:
            kerkoEmerView();
            break;
        case 3:
            adminHome(loggedAdmin);
            break;
        default:
            kerkoIdView();
    }
}

void kerkoIdView() {
    int id;

    printf("\nJu lutem fusni ID e punonjesit qe kerkoni:\n");
    printf("--------------------------------------------\n");
    printf("ID: ");

    bool vazhdo = true;
    Punonjes *tmp;
    while (vazhdo) {
        int res = scanf("%d", &id);
        while (res != 1 || id < 0) {
            getchar();
            printf("ID duhet te jete nje numer pozitiv!\n");
            printf("ID: ");
            if (id == -1) {
                printf("\n\n");
                kerkoPunonjesView();
            }
            res = scanf("%d", &id);
        }

        tmp = getPunonjesByIdReference(id);
        if (tmp == NULL) {
            printf("Duke kerkuar...\n");
            printf("Ky punonjes nuk gjendet!\n");
            printf("ID: ");
            vazhdo = true;
        } else if (tmp->id == -1) {
            printf("Duke kerkuar...\n");
            printf("Ky punonjes nuk gjendet!\n");
            printf("ID: ");
            vazhdo = true;
        } else {
            vazhdo = false;
        }
    }

    printf("Duke kerkuar...\n");
    printf("Punonjesi me ID=%d u gjend\n", tmp->id);
    printPunonjes(*tmp);
    kerkoPunonjesView();
}

void kerkoEmerView() {
    char input[50];
    printf("\nJu lutem fusni emrin e punonjesit qe kerkoni:\n");
    printf("---------------------------------------------\n");
    printf("Emri: ");

    scanf("%s", input);
    printf("Duke kerkuar...");
    pList *punonjesit = getPunonjesit();
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            char *name;
            char *tmp;

            tmp = strdup(punonjesit->punonjes.emer);
            while ((name = strsep(&tmp, "_"))) {
                if (strcasecmp(input, name) == 0) {
                    printPunonjes(punonjesit->punonjes);
                    printf("Punonjesi u gjet!\n");
                    break;
                }
            }
        }
        punonjesit = punonjesit->next;
    }

    kerkoPunonjesView();
}

void raporteView() {
    int input;

    printf("\nRaporte\n");
    printf("---------------------------------------------------------\n");
    printf("1. Listo gjithe punonjesit\n");
    printf("2. Filtro punonjesit sipas moshes\n");
    printf("3. Listo punojesit qe nuk kane raportuar punet\n");
    printf("4. Trego numrin e kryer nga cdo punonjes dhe totalin\n");
    printf("5. Mosha dhe paga mesatare e punonjesve\n");
    printf("6. ID e punonjesit qe ka kryer me shume/pak pune\n");
    printf("7. ID dhe mosha e punonjesit qe ka pagen me te ulet/larte\n");
    printf("8. ID e punonjesit me rendimentin me te larte\n");
    printf("9. Kthehu mbrapa\n");
    printf("---------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 9) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 9: ");
        res = scanf("%d", &input);
    }

    switch (input) {
        case 1:
            listoPunonjesit();
            break;
        case 2:
            listoPunonjesitMosha();
            break;
        case 3:
            listoPunonjesitRaportim();
            break;
        case 4:
            listoPunonjesitPunet();
            break;
        case 5:
            listoMesataret();
            break;
        case 6:
            listoSipasPunes();
            break;
        case 7:
            listoSipasPages();
            break;
        case 8:
            listoSipasRendimentit();
            break;
        case 9:
            adminHome(loggedAdmin);
            break;
    }
}

void listoPunonjesit() {
    printf("\nLista e gjithe punonjesve: \n");
    printf("--------------------------\n");
    printEmrat(NULL);
    printf("--------------------------\n");
    raporteView();
}

void listoPunonjesitMosha() {
    int input;
    printf("\nJu lutem fusni moshen minimale\n");
    printf("----------------------------------------\n");
    printf("Mosha: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 0) {
        getchar();
        printf("Mosha duhet te jete nje numer pozitiv!\n");
        printf("Mosha: ");
        res = scanf("%d", &input);
    }

    pList *punonjesit = getPunonjesit();

    printf("\nLista e punonjeseve me te medhenj se %d vjec: \n", input);
    printf("-----------------------------------------------\n");
    int i = 1;
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.mosha > input && punonjesit->punonjes.id != -1) {
            printf("%d) ", i);
            printf("%s", punonjesit->punonjes.emer);
            printf("  (%d vjec)\n", punonjesit->punonjes.mosha);
            i++;
        }
        punonjesit = punonjesit->next;
    }
    printf("-----------------------------------------------\n");
    raporteView();
}

void listoPunonjesitRaportim() {
    pList *punonjesit = getPunonjesit();
    printf("\nLista e punonjeseve qe skane raportuar punet: \n");
    printf("-----------------------------------------------\n");
    int i = 1;
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.raportuar == 0 && punonjesit->punonjes.id != -1) {
            printf("%d) ", i);
            printf("%s\n", punonjesit->punonjes.emer);
            i++;
        }
        punonjesit = punonjesit->next;
    }
    printf("-----------------------------------------------\n");
    raporteView();
}

void listoPunonjesitPunet() {
    int totali = 0;

    pList *punonjesit = getPunonjesit();
    printf("\nLista e punonjeseve dhe punet e kryera nga secili: \n");
    printf("--------------------------------------------------\n");
    int i = 1;
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            printf("%d) ", i);
            printf("%s", punonjesit->punonjes.emer);
            printf("     Punet: %d\n", punonjesit->punonjes.kryer);
            totali += punonjesit->punonjes.kryer;
            i++;
        }
        punonjesit = punonjesit->next;
    }
    printf("--------------------------------------------------\n");
    printf("Totali i puneve te kryera: %d\n", totali);
    printf("--------------------------------------------------\n");

    raporteView();
}

void listoMesataret() {
    float pagaTotal = 0;
    float moshaTotal = 0;
    int nrTotal = 0;

    pList *punonjesit = getPunonjesit();
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            pagaTotal += punonjesit->punonjes.paga;
            moshaTotal += punonjesit->punonjes.mosha;
            nrTotal++;
        }
        punonjesit = punonjesit->next;
    }

    printf("\n------------------------------------------\n");
    printf("Paga  mesatare e punonjesve: %.2f\n", (pagaTotal/nrTotal));
    printf("Mosha mesatare e punonjesve: %.2f\n", (moshaTotal/nrTotal));
    printf("--------------------------------------------\n");

    raporteView();
}

void listoSipasPunes() {
    int input;
    printf("\nJu lutem zgjidhni kriterin e shfaqjes:\n");
    printf("----------------------------------------\n");
    printf("1) Punonjesi me me shume pune\n");
    printf("2) Punonjesi me me pak pune\n");
    printf("3) Kthehu mbrapa\n");
    printf("----------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 3) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere midis 1 dhe 3\n");
        printf("Veprimi: ");
        res = scanf("%d", &input);
    }

    bool max = false;
    switch (input) {
        case 1:
            max = true;
            break;
        case 2:
            max = false;
            break;
        case 3:
            raporteView();
            break;
    }

    int kriter = 999999;
    if (max) {
        kriter = -999999;
    }

    int ID;
    pList *punonjesit = getPunonjesit();
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            if (max) {
                if (punonjesit->punonjes.kryer > kriter) {
                    kriter = punonjesit->punonjes.kryer;
                    ID = punonjesit->punonjes.id;
                }
            } else {
                if (punonjesit->punonjes.kryer < kriter) {
                    kriter = punonjesit->punonjes.kryer;
                    ID = punonjesit->punonjes.id;
                }
            }
        }
        punonjesit = punonjesit->next;
    }

    char *ndajfolja;
    if (max) {
        ndajfolja = "shume";
    } else {
        ndajfolja = "pak";
    }

    printf("\n----------------------------------------------------------\n");
    printf("ID e punonjesit me me %s pune eshte: %d  (nr i puneve: %d)\n", ndajfolja, ID, kriter);
    printf("----------------------------------------------------------\n");

    raporteView();
}

void listoSipasPages() {
    int input;
    printf("\nJu lutem zgjidhni kriterin e shfaqjes:\n");
    printf("----------------------------------------\n");
    printf("1) Punonjesi me pagen me te larte\n");
    printf("2) Punonjesi me pagen me te ulet\n");
    printf("3) Kthehu mbrapa\n");
    printf("----------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 3) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere midis 1 dhe 3\n");
        printf("Veprimi: ");
        res = scanf("%d", &input);
    }

    bool max = false;
    switch (input) {
        case 1:
            max = true;
            break;
        case 2:
            max = false;
            break;
        case 3:
            raporteView();
            break;
    }

    float kriter = 999999;
    if (max) {
        kriter = -999999;
    }

    int ID;
    pList *punonjesit = getPunonjesit();
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            if (max) {
                if (punonjesit->punonjes.paga > kriter) {
                    kriter = punonjesit->punonjes.paga;
                    ID = punonjesit->punonjes.id;
                }
            } else {
                if (punonjesit->punonjes.paga < kriter) {
                    kriter = punonjesit->punonjes.paga;
                    ID = punonjesit->punonjes.id;
                }
            }
        }
        punonjesit = punonjesit->next;
    }

    char *ndajfolja;
    if (max) {
        ndajfolja = "larte";
    } else {
        ndajfolja = "ulet";
    }

    printf("\n------------------------------------------------------------------\n");
    printf("ID e punonjesit me pagen me te %s eshte: %d  (paga: %.2f)\n", ndajfolja, ID, kriter);
    printf("------------------------------------------------------------------\n");

    raporteView();
}

void listoSipasRendimentit() {
    int input;
    printf("\nJu lutem zgjidhni kriterin e shfaqjes:\n");
    printf("------------------------------------------\n");
    printf("1) Punonjesi me rendimentin me te larte\n");
    printf("2) Punonjesi me rendimentin me te ulet\n");
    printf("3) Kthehu mbrapa\n");
    printf("------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 3) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere midis 1 dhe 3\n");
        printf("Veprimi: ");
        res = scanf("%d", &input);
    }

    bool max = false;
    switch (input) {
        case 1:
            max = true;
            break;
        case 2:
            max = false;
            break;
        case 3:
            raporteView();
            break;
    }

    float kriter = 999999;
    if (max) {
        kriter = -999999;
    }

    int ID;
    int ardhur;
    int kryer;
    float rendiment;
    pList *punonjesit = getPunonjesit();
    while(punonjesit != NULL) {
        if (punonjesit->punonjes.id != -1) {
            if (punonjesit->punonjes.ardhur > 0) {
                rendiment = ((float) punonjesit->punonjes.kryer / (float) punonjesit->punonjes.ardhur);
            } else {
                rendiment = 0;
            }
            if (max) {
                if (rendiment > kriter) {
                    ardhur = punonjesit->punonjes.ardhur;
                    kryer = punonjesit->punonjes.kryer;
                    kriter = rendiment;
                    ID = punonjesit->punonjes.id;
                }
            } else {
                if (rendiment < kriter) {
                    ardhur = punonjesit->punonjes.ardhur;
                    kryer = punonjesit->punonjes.kryer;
                    kriter = rendiment;
                    ID = punonjesit->punonjes.id;
                }
            }
        }
        punonjesit = punonjesit->next;
    }

    char *ndajfolja;
    if (max) {
        ndajfolja = "larte";
    } else {
        ndajfolja = "ulet";
    }

    printf("\n-------------------------------------------------------------------------------\n");
    printf("ID e punonjesit me rendimentin me te %s eshte: %d  (rendimenti: %.2f (%d/%d))\n", ndajfolja, ID, kriter, kryer, ardhur);
    printf("-------------------------------------------------------------------------------\n");

    raporteView();
}