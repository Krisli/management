//punonjes
// Created by Ejsi Veshaj and Ariola Leka on 6/3/2018.
//

#ifndef MANAGEMENT_MAIN_H
#define MANAGEMENT_MAIN_H

#include "file.h"
#include "file.h"
#include "functions.h"
#include "punonjes.h"
#include "punonjesView.h"
#include "admin.h"

bool loggedIn;
Punonjes loggedUser;
Admin loggedAdmin;

void logout();
void updateLoggedUser(Punonjes user);

#endif //MANAGEMENT_MAIN_H
