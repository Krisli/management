//
// Created by Ejsi on 18-06-01.
//

#ifndef MANAGEMENT_PUNONJES_H
#define MANAGEMENT_PUNONJES_H

typedef struct Punonjes {
    int id;
    char emer[50];
    char fjalekalim[255];
    int mosha;
    float paga;
    int raportuar;
    int ardhur;
    int kryer;
} Punonjes;

typedef struct node {
    Punonjes punonjes;
    struct node *next;
    struct node *prev;
} pList;

int lastId;

pList *newNode(Punonjes data);
pList *addNodeToHead(pList *psList, Punonjes data);
pList *getPunonjesit();

Punonjes getPunonjes(int ID, char passw[]);
Punonjes shtoPunonjes(char emer[], char mbiemer[], char fjalekalim[], int mosha, float paga, int raportuar, int ardhur, int kryer);
Punonjes getPunonjesById(int ID);
Punonjes *getPunonjesByIdReference(int ID);

void printPunonjes(Punonjes pun);
void printPunonjesit(pList *punonjesit);
void printEmrat(pList *punonjesit);

bool savePunonjesit();
bool deletePunonjesById(int ID);
bool ndryshoEmrin(int ID, char emri[]);
bool ndryshoFjalekalimin(int ID, char passw[]);
bool ndryshoMoshen(int ID, int mosha);
bool ndryshoPagen(int ID, float paga);
bool ndryshoRaportinArdhur(int ID, int raporti);
bool ndryshoRaportinKryera(int ID, int raporti);

#endif //MANAGEMENT_PUNONJES_H
