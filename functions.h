//
// Created by Ejsi on 6/2/2018.
//

#ifndef MANAGEMENT_FUNCTIONS_H
#define MANAGEMENT_FUNCTIONS_H

int parseInt(char* str, int n);
float parseFloat(char* str);
char * strsep(char **stringp, const char *delim);
char* strReplace(char* str, char find, char replace);

#endif //MANAGEMENT_FUNCTIONS_H
