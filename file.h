//
// Created by Ejsi on 18-06-01.
//

#ifndef MANAGEMENT_FILE_H
#define MANAGEMENT_FILE_H

bool createConnection(char fileName[], char operation);
void closeConnection();
void loadBuffer();
char* getBuffer();
void saveToFile(char input[]);

#endif //MANAGEMENT_FILE_H
