//
// Created by Ejsi on 18-06-04.
//

#ifndef MANAGEMENT_ADMIN_H
#define MANAGEMENT_ADMIN_H

typedef struct Admin {
    int id;
    char fjalekalim[255];
} Admin;

bool adminHome(Admin admin);
Admin getAdmin(char password[]);

#endif //MANAGEMENT_ADMIN_H
