//
// Created by Ejsi on 6/3/2018.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "main.h"

Punonjes loggedUser;

void ndryshoFjalekaliminView(char oldPassw[]);
void raportiPunetArdhuraView();
void raportiPunetKryeraView();

bool punonjesHome(Punonjes punonjes) {
    int input;
    loggedUser = punonjes;
    printf("\n\nMiresevjen %s!\n", strReplace(punonjes.emer, *"_", *" "));
    printf("-------------------------------------------------------------\n");
    printf("1. Ndrysho Fjalekalimin\n");
    printf("2. Regjistro numrin e puneve te ardhura per cdo dite te javes\n");
    printf("3. Raporto numrin e puneve te kryera per cdo dite\n");
    printf("4. Dil\n");
    printf("-------------------------------------------------------------\n");
    printf("Veprimi: ");

    int res = scanf("%d", &input);
    while (res != 1 || input < 1 || input > 4) {
        getchar();
        printf("Ju lutem zgjidhni nje vlere nga 1 ne 4: ");
        res = scanf("%d", &input);
    }

    switch (input) {
        case 1:
            ndryshoFjalekaliminView(punonjes.fjalekalim);
            break;
        case 2:
            raportiPunetArdhuraView();
            break;
        case 3:
            raportiPunetKryeraView();
            break;
        case 4:
            logout();
            break;
    }

    return true;
}

void ndryshoFjalekaliminView(char oldPassw[]) {
    char passw[] = "";
    printf("\nNdryshim i fjalekalimit\n");
    printf("-------------------------\n");
    printf("Fjalekalim i ri: ");
    scanf("%s", passw);
    if (strcmp(oldPassw, passw) == 0) {
        printf("Fjalekalimi qe vendoset eshte i njejti me fjalekalimin ekzistues.\n");
        printf("Fjalekalimi nuk u ndryshua.\n");
    } else {
        if (ndryshoFjalekalimin(loggedUser.id, passw)) {
            printf("Fjalekalimi u ndryshua me sukses!\n");
        } else {
            printf("Ndodhi nje problem gjate ndryshimit te fjalekalimit!\n");
        }
    }

    punonjesHome(loggedUser);
    return;
}

void raportiPunetArdhuraView() {
    int punet[5];
    int i = 0;

    printf("Ju lutem fusni numrin e puneve te ardhura per secilen dite:\n");
    printf("-----------------------------------------------------------\n");

    for (i = 0; i < 5; i++) {
        switch (i) {
            case 0:
                printf("E hene: ");
                break;
            case 1:
                printf("E marte: ");
                break;
            case 2:
                printf("E merkure: ");
                break;
            case 3:
                printf("E enjte: ");
                break;
            case 4:
                printf("E premte: ");
                break;
        }
        int res = scanf("%d", &punet[i]);
        while (res != 1 || punet[i] < 0) {
            getchar();
            printf("Raporti duhet te jete nje numer pozitiv!\n");
            res = scanf("%d", &punet[i]);
        }
    }

    int shuma = 0;
    for (i = 0; i < 5;i++) {
        shuma += punet[i];
    }
    printf("Totali i puneve te ardhura: %d", shuma);
    ndryshoRaportinArdhur(loggedUser.id, shuma);

    punonjesHome(loggedUser);
    return;
}

void raportiPunetKryeraView() {
    int punet[5];
    int i = 0;

    printf("Ju lutem fusni numrin e puneve te kryera per secilen dite:\n");
    printf("----------------------------------------------------------\n");

    for (i = 0; i < 5; i++) {
        switch (i) {
            case 0:
                printf("E hene: ");
                break;
            case 1:
                printf("E marte: ");
                break;
            case 2:
                printf("E merkure: ");
                break;
            case 3:
                printf("E enjte: ");
                break;
            case 4:
                printf("E premte: ");
                break;
        }
        int res = scanf("%d", &punet[i]);
        while (res != 1 || punet[i] < 0) {
            getchar();
            printf("Raporti duhet te jete nje numer pozitiv!\n");
            res = scanf("%d", &punet[i]);
        }
    }

    int shuma = 0;
    for (i = 0; i < 5;i++) {
        shuma += punet[i];
    }
    printf("Totali i puneve te kryera: %d", shuma);
    ndryshoRaportinKryera(loggedUser.id, shuma);

    punonjesHome(loggedUser);
    return;
}