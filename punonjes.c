//
// Created by Ejsi on 18-06-01.
//

#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>
#include <string.h>
#include "main.h"

char *bPunonjesit;
pList *punList;
int lastId;

pList *getPunonjesit();
Punonjes getPunonjesById(int ID);
pList *newNode(Punonjes data);
pList *addNodeToHead(pList *psList, Punonjes data);
void printPunonjes(Punonjes pun);
void printPunonjesit(pList *punonjesit);
void printEmrat(pList *punonjesit);
bool savePunonjesit();

Punonjes getPunonjes(int ID, char passw[]) {
    Punonjes punonjes;

    punonjes = getPunonjesById(ID);
    if (strcmp(punonjes.fjalekalim, passw) != 0) {
        punonjes.id = -2;
        return punonjes;
    }

    return punonjes;
}

pList *getPunonjesit() {
    char *fPunonjesit;
    char *fFields;

    createConnection("punonjes.txt", 'r');
    loadBuffer();
    closeConnection();
    bPunonjesit = getBuffer();

    char *file = strdup(bPunonjesit);
    file = strtok(file, "#");

    pList *root = NULL;
    root = (pList*) malloc(sizeof(pList));
    root->punonjes.id = -1;
    pList *head = root;

    Punonjes pun;
    int field = 0;
    while ((fPunonjesit = strsep(&file, "|"))) {
        if (strlen(fPunonjesit) > 0) {
            while ((fFields = strsep(&fPunonjesit, "~"))) {
                if (strlen(fFields) > 0) {
                    switch (field) {
                        case 0:
                            pun.id = parseInt(fFields, strlen(fFields));
                            lastId = pun.id;
                            break;
                        case 1:
                            strcpy(pun.emer, fFields);
                            break;
                        case 2:
                            strcpy(pun.fjalekalim, fFields);
                            break;
                        case 3:
                            pun.mosha = parseInt(fFields, strlen(fFields));
                            break;
                        case 4:
                            pun.paga = parseFloat(fFields);
                            break;
                        case 5:
                            pun.raportuar = parseInt(fFields, strlen(fFields));
                            break;
                        case 6:
                            pun.ardhur = parseInt(fFields, strlen(fFields));
                            break;
                        case 7:
                            pun.kryer = parseInt(fFields, strlen(fFields));
                            break;
                    }
                    field++;
                }
            }
            field = 0;
            addNodeToHead(head, pun);
            head = head->next;
        }
    }
    punList = root;
    return punList;
}

pList *newNode(Punonjes data) {
    pList *psList = (pList *) malloc(sizeof(pList));
    if(psList == NULL) {
        printf("DEBUG : Gabim gjate shtimit te nyjes se re!\n");
        return NULL;
    }
    psList->punonjes = data;
    psList->next = NULL;
    return psList;
}

pList *addNodeToHead(pList *psList, Punonjes data) {
    pList *psListNew = newNode(data);
    if(psListNew == NULL) {
        printf("DEBUG : Gabim gjate shtimit te nyjes!\n");
        return NULL;
    }
    psList->next = psListNew;
    psListNew->prev = psList;
    return psListNew;
}

void printPunonjes(Punonjes pun) {
    printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("ID: %d\n", pun.id);
    printf("Emri: %s\n", pun.emer);
    printf("Fjalekalimi: *******\n");
    printf("Mosha: %d\n", pun.mosha);
    printf("Paga: %f\n", pun.paga);
    printf("Raportuar: %d\n", pun.raportuar);
    printf("Punet e ardhura: %d\n", pun.ardhur);
    printf("Punet e kryera: %d\n", pun.kryer);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}

void printPunonjesit(pList *punonjesit) {
    if(punonjesit == NULL) {
        punonjesit = getPunonjesit();
    }

    if (punonjesit->punonjes.id == -1) {
        punonjesit = punonjesit->next;
    }

    int i = 0;
    while(punonjesit != NULL) {
        printPunonjes(punonjesit->punonjes);
        punonjesit = punonjesit->next;
        i++;
    }
}

void printEmrat(pList *punonjesit) {
    if(punonjesit == NULL) {
        punonjesit = getPunonjesit();
    }

    if (punonjesit->punonjes.id == -1) {
        punonjesit = punonjesit->next;
    }

    int i = 1;
    while(punonjesit != NULL) {
        printf("%d) %s \n", i, punonjesit->punonjes.emer);
        punonjesit = punonjesit->next;
        i++;
    }
}

Punonjes getPunonjesById(int ID) {
    punList = getPunonjesit();

    pList *head;
    head = punList->next;
    while (head != NULL) {
        if (head->punonjes.id == ID) {
            return head->punonjes;
        }
        head = head->next;
    }

    Punonjes nullPun;
    nullPun.id = -1;

    return nullPun;
}

Punonjes *getPunonjesByIdReference(int ID) {
    punList = getPunonjesit();

    pList *head;
    head = punList->next;
    while (head != NULL) {
        if (head->punonjes.id == ID) {
            return &head->punonjes;
        }
        head = head->next;
    }

    Punonjes nullPun;
    nullPun.id = -1;

    return &nullPun;
}

pList *getNodeById(int ID) {
    punList = getPunonjesit();

    pList *head;
    head = punList->next;
    while (head != NULL) {
        if (head->punonjes.id == ID) {
            return head;
        }
        head = head->next;
    }

    return punList;
}

bool deletePunonjesById(int ID) {
    pList node = *getNodeById(ID);

    if (node.punonjes.id == -1) {
        return false;
    }

    pList *prev = node.prev;
    pList *next = node.next;
    prev->next = next;

    savePunonjesit();

    return true;
}

Punonjes shtoPunonjes(char emer[], char mbiemer[], char fjalekalim[],int mosha, float paga, int raportuar, int ardhur, int kryer) {
    Punonjes new;
    punList = getPunonjesit();

    char name[50] = "";
    strcat(name, emer);
    strcat(name, "_");
    strcat(name, mbiemer);
    new.id = ++lastId;
    strcpy(new.emer, name);
    strcpy(new.fjalekalim, fjalekalim);
    new.mosha = mosha;
    new.paga = paga;
    new.raportuar = raportuar;
    new.ardhur = ardhur;
    new.kryer = kryer;

    pList *head;
    head = punList->next;
    while (head->next != NULL) {
        head = head->next;
    }

    pList *tail = NULL;
    tail = (pList*) malloc(sizeof(pList));
    if(tail == NULL) {
        printf("DEBUG : Gabim gjate shtimit te nyjes se re!\n");
        new.id = -1;
        return new;
    }
    tail->punonjes = new;
    tail->prev = head;
    tail->next =  NULL;
    head->next = tail;

    return new;
}

bool ndryshoEmrin(int ID, char emri[]) {
    Punonjes *pun = getPunonjesByIdReference(ID);

    memset(&pun->emer[0], 0, sizeof(pun->emer));
    memcpy(pun->emer, emri, strlen(emri));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool ndryshoFjalekalimin(int ID, char passw[]) {
    Punonjes *pun = getPunonjesByIdReference(ID);

    memset(&pun->fjalekalim[0], 0, sizeof(pun->fjalekalim));
    memcpy(pun->fjalekalim, passw, strlen(passw));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool ndryshoMoshen(int ID, int mosha) {
    Punonjes *pun = getPunonjesByIdReference(ID);

    memset(&pun->mosha, 0, sizeof(pun->mosha));
    memcpy(&pun->mosha, &mosha, sizeof(&mosha));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool ndryshoPagen(int ID, float paga) {
    Punonjes *pun = getPunonjesByIdReference(ID);

    memset(&pun->paga, 0, sizeof(pun->paga));
    memcpy(&pun->paga, &paga, sizeof(&paga));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool ndryshoRaportinArdhur(int ID, int raporti) {
    Punonjes *pun = getPunonjesByIdReference(ID);

    memset(&pun->ardhur, 0, sizeof(pun->ardhur));
    memcpy(&pun->ardhur, &raporti, sizeof(raporti));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool ndryshoRaportinKryera(int ID, int raporti) {
    Punonjes *pun = getPunonjesByIdReference(ID);
    int raportuar = 1;

    memset(&pun->kryer, 0, sizeof(pun->kryer));
    memcpy(&pun->kryer, &raporti, sizeof(raporti));

    memset(&pun->raportuar, 0, sizeof(pun->raportuar));
    memcpy(&pun->raportuar, &raportuar, sizeof(raportuar));

    savePunonjesit();
    updateLoggedUser(*pun);

    return true;
}

bool savePunonjesit() {
    char result[999] = "";

    pList *head = punList;

    if (head->punonjes.id == -1) {
        head = head->next;
    }
    int i =0;
    while (head != NULL) {
        char tmp[255];
        sprintf(tmp, "%d", head->punonjes.id);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        strcat(result, head->punonjes.emer);
        strcat(result, "~");
        strcat(result, head->punonjes.fjalekalim);
        strcat(result, "~");
        sprintf(tmp, "%d", head->punonjes.mosha);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        sprintf(tmp, "%f", head->punonjes.paga);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        sprintf(tmp, "%d", head->punonjes.raportuar);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        sprintf(tmp, "%d", head->punonjes.ardhur);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "~");
        sprintf(tmp, "%d", head->punonjes.kryer);
        strcat(result, tmp);
        memset(&tmp[0], 0, sizeof(tmp));
        strcat(result, "|");
        head = head->next;
        i++;
    }
    strcat(result, "#");

    createConnection("punonjes.txt",'w');
    saveToFile(result);
    closeConnection();

    return true;
}