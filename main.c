//
// Created by Ejsi on 18-06-01.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include "main.h"

bool isAdmin = false;
bool loggedIn = false;
Punonjes loggedUser;
Admin loggedAdmin;

bool login();

int main() {
    bool res = login();
    while (!res) {
        char input[2];
        printf("Deshironi te riktheheni ne menune kryesore? [Y/N]\n");
        getchar();
        scanf("%s", input);
        if (strcmp(input, "Y") == 0 || strcmp(input, "y") == 0) {
            res = login();
        } else {
            printf("Programi po mbyllet...\n");
            return 0;
        }
    }

    if (!isAdmin) {
        punonjesHome(loggedUser);
    } else {
        adminHome(loggedAdmin);
    }

    return 0;
}

bool login() {
    int id;
    char passw[255];

    printf("\nMiresevini ne programin e menaxhimit te punonjesve!\n\n");
    printf("Ju lutem identifikohuni\n");
    printf("---------------------------------------------------\n");
    printf("ID: ");

    int res = scanf("%d", &id);
    while (res != 1 || id < 0) {
        getchar();
        if (id == -1) {
            printf("Programi po mbyllet...\n");
            exit(0);
        }
        printf("ID duhet te jete nje numer pozitiv!\n");
        printf("ID: ");
        res = scanf("%d", &id);
    }

    printf("Fjalekalimi: ");
    scanf("%s", passw);
    printf("\nDuke ju identifikuar...\n");

    if (id != 0) {
        Punonjes result = getPunonjes(id, passw);
        if (result.id > 0) {
            loggedUser = result;
            loggedIn = true;
            isAdmin = false;
            printf("Identifikimi u krye me sukses!\n");
        } else if (result.id == -1) {
            printf("ID e dhene nuk ekziston!\n");
            return false;
        } else if (result.id == -2) {
            printf("Fjalekalimi eshte i pasakte!\n");
            return false;
        }
    } else {
        Admin tmp = getAdmin(passw);
        if (tmp.id == 0) {
            printf("Identifikimi per adminin u krye me sukses!\n");
            isAdmin = true;
            loggedAdmin = tmp;
        } else {
            printf("Fjalekalimi eshte i pasakte!\n");
            return false;
        }
    }

    return true;
}

void logout() {
    loggedIn = false;
    main();
}

void updateLoggedUser(Punonjes user) {
    loggedUser = user;
}